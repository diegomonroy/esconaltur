<!-- Begin Banner -->
	<section class="banner wow bounceInUp" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php
				if ( is_page( 'quienes-somos' ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif;
				if ( is_page( 'vision-mision' ) ) : dynamic_sidebar( 'banner_vision_mision' ); endif;
				if ( is_page( 'valores' ) ) : dynamic_sidebar( 'banner_valores' ); endif;
				if ( is_page( 'politicas-de-calidad' ) ) : dynamic_sidebar( 'banner_politicas_de_calidad' ); endif;
				?>
			</div>
		</div>
	</section>
<!-- End Banner -->