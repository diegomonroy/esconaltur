<!-- Begin Banner -->
	<section class="banner wow bounceInUp" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php
				if ( is_page( 'servicios' ) ) : dynamic_sidebar( 'banner_servicios' ); endif;
				if ( is_page( 'escolar' ) ) : dynamic_sidebar( 'banner_escolar' ); endif;
				if ( is_page( 'empresarial' ) ) : dynamic_sidebar( 'banner_empresarial' ); endif;
				if ( is_page( 'turismo' ) ) : dynamic_sidebar( 'banner_turismo' ); endif;
				?>
			</div>
		</div>
	</section>
<!-- End Banner -->