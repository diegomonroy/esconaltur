<?php
if ( is_front_page() ) :
	get_template_part( 'part', 'banner-home' );
endif;
if ( is_page( array( 'quienes-somos', 'vision-mision', 'valores', 'politicas-de-calidad' ) ) ) :
	get_template_part( 'part', 'banner-quienes-somos' );
endif;
if ( is_page( array( 'servicios', 'escolar', 'empresarial', 'turismo' ) ) ) :
	get_template_part( 'part', 'banner-servicios' );
endif;
if ( is_page( 'calidad' ) ) :
	get_template_part( 'part', 'banner-calidad' );
endif;
if ( is_page( 'modulo-asociados' ) ) :
	get_template_part( 'part', 'banner-modulo-asociados' );
endif;
if ( is_page( 'contactenos' ) ) :
	get_template_part( 'part', 'banner-contactenos' );
endif;
?>
<?php if ( is_page( array( 'servicios', 'escolar', 'empresarial', 'turismo' ) ) ) : else : ?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->
<?php endif; ?>