<?php if ( is_page( array( 'servicios', 'escolar', 'empresarial', 'turismo' ) ) ) : else : ?>
<!-- Begin Home -->
	<section class="home wow bounceInRight" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home' ); ?>
			</div>
		</div>
	</section>
<!-- End Home -->
<?php endif; ?>