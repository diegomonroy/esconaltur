<!-- Begin Logos -->
	<section class="logos wow bounceInLeft" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="moduletable_lo1">
					<h2 class="text-center">RESPALDO Y SOLIDEZ</h2>
					<?php echo do_shortcode( '[tc-owl-carousel carousel_cat="respaldo-y-solidez" order="ASC"]' ); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Logos -->