<!-- Begin Service -->
	<section class="service wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'service' ); ?>
			</div>
		</div>
	</section>
<!-- End Service -->