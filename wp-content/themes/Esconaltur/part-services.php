<?php if ( is_page( array( 'escolar', 'empresarial', 'turismo' ) ) ) : ?>
<!-- Begin Services -->
	<section class="service wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php
				if ( is_page( 'escolar' ) ) :
					dynamic_sidebar( 'escolar' );
				endif;
				if ( is_page( 'empresarial' ) ) :
					dynamic_sidebar( 'empresarial' );
				endif;
				if ( is_page( 'turismo' ) ) :
					dynamic_sidebar( 'turismo' );
				endif;
				?>
			</div>
		</div>
	</section>
<!-- End Services -->
<?php endif; ?>