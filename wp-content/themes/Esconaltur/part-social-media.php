<!-- Begin Social Media -->
	<section class="social_media wow fadeInUp" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
	</section>
<!-- End Social Media -->